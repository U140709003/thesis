package com.example.esra.sendmessage;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class login extends AppCompatActivity implements View.OnClickListener{

    EditText uname,pass;
    Button btn;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btn = findViewById(R.id.btnLogin);
        uname = findViewById(R.id.userName);
        pass = findViewById(R.id.password);
    }

    @Override
    public void onClick(View v) {

    }
    public void onClickButton(View v){
        if(uname.getText().toString().equalsIgnoreCase("passenger") && pass.getText().toString().equals("123")){
            Intent intent = new Intent(context,MainActivity.class);
            startActivity(intent);
        }else if(uname.getText().toString().equalsIgnoreCase("driver") && pass.getText().toString().equals("123")){
            Intent intent = new Intent(context,NFCDisplayActivity.class);
            startActivity(intent);
        }
        else{
            Toast.makeText(context,"Login failed",Toast.LENGTH_LONG);
        }
    }
}
