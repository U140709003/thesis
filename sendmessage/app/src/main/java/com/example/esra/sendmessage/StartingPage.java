package com.example.esra.sendmessage;

import android.content.Context;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StartingPage extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback{
    EditText mEditText;
    Context con =this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting_page);

        Button btn1 = (Button) findViewById(R.id.btn1);
        Button btn2 = (Button) findViewById(R.id.btn2);

        Log.d("NFCDemo", "Getting Adapter" );
        NfcAdapter mAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mAdapter == null) {
            mEditText.setText("Sorry this device does not have NFC.");
            Log.e("NFCDemo", "Adatper is null" );
            return;
        }

        if (!mAdapter.isEnabled()) {
            Log.e("NFCDemo", " Adapter ıs dısabled" );
            Toast.makeText(this, "Please enable NFC via Settings.", Toast.LENGTH_LONG).show();
        }
        Log.d("NFCDemo", " setNdefPushMessageCallback" );
        mAdapter.setNdefPushMessageCallback(this, this);
        Log.d("NFCDemo", " setNdefPushMessageCallback successful" );
    }
    public void onClickButton(View v ){
        Intent intent = new Intent(con,login.class);
        startActivity(intent);
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        return null;
    }
}
